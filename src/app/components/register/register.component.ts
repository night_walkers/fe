import { StorageService } from '../../providers/storage.service';
import { RootService } from '../../providers/root.service';
import { ApiService } from '../../providers/api.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  private user = {
    email: '',
    password: '',
    username: ''
  };
  constructor(
    private storage: StorageService,
    private root: RootService,
    private api: ApiService,
    private router: Router
  ) { }

  private onSubmit = () => {
    this.root.loading = true;
    this.api.post('customers', {
      'email': this.user.email,
      'password': this.user.password,
      'username': this.user.username
    }).subscribe((test) => {
      this.api.post('customers/login', {
        'email': this.user.email,
        'password': this.user.password
      }).subscribe((data) => {
        this.storage.setStorage('sea_messages', data);
        this.root.loading = false;
        this.router.navigate(['account']);
      }, (fail) => {
        console.log(fail);
      });
    }, (fail) => {
      console.log(fail);
    });
  }

}
