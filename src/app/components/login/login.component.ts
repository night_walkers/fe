import { StorageService } from '../../providers/storage.service';
import { RootService } from '../../providers/root.service';
import { ApiService } from '../../providers/api.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
// [TODO] set validation for inputs.
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  private user = {
    email: '',
    password: ''
  };
  constructor(
    private storage: StorageService,
    private root: RootService,
    private api: ApiService,
    private router: Router
  ) { }

  private onSubmit = () => {
    // this.root.loading = true;
    // this.api.post('customers/login', {
    //   'email': this.user.email,
    //   'password': this.user.password
    // }).subscribe((data) => {
    //   this.storage.setStorage('sea_messages', data);
    //   this.root.loading = false;
    //   this.router.navigate(['account']);
    // }, (fail) => {
    //   console.log(fail);
    // });
  }
}
