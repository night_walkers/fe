import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { CoreComponent } from './components/account/core/core.component';
import { LoginComponent } from './components/login/login.component';
import { RouteguardService } from './providers/routeguard.service';
import { HomeComponent } from './components/home/home.component';
import { StorageService } from './providers/storage.service';
import { AppRoutingModule } from './/app-routing.module';
import { ApiService } from './providers/api.service';
import { AppComponent } from './app.component';
import { RootService } from './providers/root.service';
import { RegisterComponent } from './components/register/register.component';
import { WallComponent } from './components/account/wall/wall.component';
import { RandomMovementDirective } from './directives/random-movement.directive';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    CoreComponent,
    RegisterComponent,
    WallComponent,
    RandomMovementDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    AppRoutingModule
  ],
  providers: [
    ApiService,
    StorageService,
    RouteguardService,
    RootService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
