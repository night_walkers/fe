import { Directive, ElementRef, HostListener, ViewChild, AfterViewInit } from '@angular/core';
declare var $: any;

@Directive({
  selector: '[appRandomMovement]'
})
export class RandomMovementDirective implements AfterViewInit {

  constructor(private el: ElementRef) { }
  ngAfterViewInit() {
    this.animateDiv();
  }
  private animateDiv = () => {
    const newq = this.makeNewPosition();
    const speed = this.calcSpeed([this.el.nativeElement.offsetTop, this.el.nativeElement.offsetLeft], newq);
    $('.rm_div').animate({
      top: newq[0],
      left: newq[1]
    }, speed, this.animateDiv);
  }

  private calcSpeed = (prev, next) => {
    const x = Math.abs(prev[1] - next[1]);
    const y = Math.abs(prev[0] - next[0]);
    const greatest = x > y ? x : y;
    const speedModifier = 0.1;
    const speed = Math.ceil(greatest / speedModifier);
    return speed;
  }

  private makeNewPosition = () => {
    const h = $(window).height() - 10;
    const w = $(window).width() - 10;
    const nh = Math.floor(Math.random() * h);
    const nw = Math.floor(Math.random() * w);
    return [nh, nw];
  }

}
