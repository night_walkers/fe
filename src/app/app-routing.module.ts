import { RegisterComponent } from './components/register/register.component';
import { WallComponent } from './components/account/wall/wall.component';
import { CoreComponent } from './components/account/core/core.component';
import { LoginComponent } from './components/login/login.component';
import { RouteguardService } from './providers/routeguard.service';
import { HomeComponent } from './components/home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  // { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '', component: HomeComponent },
  { path: 'login', canActivate: [RouteguardService], component: LoginComponent },
  { path: 'signup', canActivate: [RouteguardService], component: RegisterComponent },
  {
    path: 'account',
    component: CoreComponent,
    canActivateChild: [RouteguardService],
    children: [
      { path: '', redirectTo: 'wall', pathMatch: 'full' },
      { path: 'wall', component: WallComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
