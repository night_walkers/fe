import { TranslateService } from '@ngx-translate/core';
import { Component } from '@angular/core';
import { RootService } from './providers/root.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private translate: TranslateService, private root: RootService) {
    // this.root.loading = false;
    translate.setDefaultLang('en');
    setTimeout(() => {
      translate.use('fr');
    }, 5000);
  }
}
