import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

  constructor() { }

  public setStorage = (key: string, value: object) => {
    if (localStorage.getItem(key)) {
      return;
    } else {
      localStorage.setItem(key, JSON.stringify(value));
    }
  }

  public getStorage = (key: string) => {
    const storage = JSON.parse(localStorage.getItem('sea_messages'));
    try {
      if (storage[key]) {
        return storage[key];
      }
    } catch (error) {
      return false;
    }
  }
}
