import { CanActivate, Router, CanActivateChild } from '@angular/router';
import { StorageService } from './storage.service';
import { Injectable } from '@angular/core';

@Injectable()
export class RouteguardService implements CanActivate, CanActivateChild {

  constructor(private storage: StorageService, private router: Router) { }

  canActivate() {
    if (!this.isLoggedIn()) {
      return true;
    } else {
      // [TODO] navigate router to wall page if logged in
      this.router.navigate(['account']);
    }
  }

  canActivateChild() {
    if (this.isLoggedIn()) {
      return true;
    } else {
      this.router.navigate(['']);
    }
  }


  private isLoggedIn = (): boolean => {
    if (this.storage.getStorage('id')) {
      return true;
    } else {
      return false;
    }
  }
}
