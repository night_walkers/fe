import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class ApiService {
  private _apiBase: string;
  private _defaultHttpOptions: object;
  private endpoints = {
    isRegistered: 'is-registered'
  };

  constructor(private http: HttpClient) {
    this._apiBase = environment._api.url;
    this._defaultHttpOptions = {
      headers: new HttpHeaders({
        'Content-Type': environment._api.contentType
      })
    };
  }

  public post(endpoint: string, body: any): Observable<any> {
    const url: string = [this._apiBase, endpoint].join('/').toLowerCase();
    return this.http.post(url, body, this._defaultHttpOptions);
  }

  public get(endpoint: string): Observable<any> {
    const url: string = [this._apiBase, endpoint].join('/').toLowerCase();
    return this.http.post(url, this._defaultHttpOptions);
  }

}
